# NLP Deep Learning Benchmarking
### Benchmarking various Deep Learning methods for sentiment classification  
A set of Jupyter notebooks containing sentiment classifiers leveraging a variety of technologies. These range from VADER to bidirectional LSTMs.

Clone this repo into a parent directory that contains the data.  
The data can be found here: https://drive.google.com/drive/folders/0Bz8a_Dbh9Qhbfll6bVpmNUtUcFdjYmF2SEpmZUZUcVNiMUw1TWN6RDV3a0JHT3kxLVhVR2M

----------
### Results

#### VADER   
* Final accuracy: 70.1%  
* AUROC: 83.0%  

#### Convolutional neural network   
* Final accuracy: 94.5%  
* AUROC: 98.7%  

#### Simple LSTM
* Final accuracy: 94.6%  
* AUROC: 98.7%

----------
### Notes

I tried three ways of saving:  
1.  Text file  
2.  Pickle  
3.  Json  
  
The code for all three is in the repo, but the json save is the fastest and cleanest **by far**.

The Razer is a bit quicker when doing CPU computations in parallel (235 sec vs 360 sec approx.) than the Mac.  

#### CNNs
It turns out that indexing the words starting at 1 (-ish, you need to leave space for the special characters) is absolutely critical!!  
The classifier worked best with stopwords left in.  
Doubling the size of the vocab (from 5k to 10k) gave a big bump in performance, pushing accuracy up by 2%. However, doubling the size of the vocab again (to 20k) didn't really take us any further.   

#### LSTMs
The simple LSTM is much slower to train than the CNN and uses about 1/2 of the volatile GPU capacity; not sure why?  



